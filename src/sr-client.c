#include <glib.h>
#include "sourcerank.h"
#include <stdio.h>
#include <math.h>
#include <gio/gio.h>
#include <string.h>
#include <dbus/dbus-glib-bindings.h>
static DBusGProxy *proxy;
static DBusGConnection *connection;
static GError *error = NULL;

#include "sr-client-glue.h"

static glong current_time (void)
{
  GTimeVal timeval;
  g_get_current_time (&timeval);
  return timeval.tv_sec;
}

void
sr_init (void)
{
  connection = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
  if (connection == NULL)
    {
      g_warning ("Unable to connect to dbus: %s\n", error->message);
      g_error_free (error);
      return;
    }

  proxy = dbus_g_proxy_new_for_name (connection,
                                     "org.meego.SourceRank",
                                     "/org/meego/SourceRank",
                                     "org.meego.SourceRank");
}

void
sr_configure (const char *engine,
              const char *param,
              float       value)
{
  org_meego_SourceRank_configure_async (proxy, engine, param, value, NULL, NULL);
}

void
sr_add_item (const char *engine,
             const char *source,
             const char *item,
             glong       time)
{
  org_meego_SourceRank_add_item_async (proxy, engine, source, item, time, NULL, NULL);
}

void
sr_item_add_karma_with_time (const char *item,
                             float       karma_amount,
                             glong       time)
{
  org_meego_SourceRank_item_add_karma_with_time_async (proxy, item, karma_amount, time, NULL, NULL);
}

void sr_add_karma (const char *item,
                   float       karma)
{
  sr_item_add_karma_with_time (item, karma, current_time ());
}


void
sr_source_add_karma (const char *engine,
                     const char *source,
                     float       karma_amount)
{
  org_meego_SourceRank_source_add_karma_async (proxy, engine,
                                               source, karma_amount,
                                               NULL, NULL);
}

void
sr_item_consume (const char *item)
{
  org_meego_SourceRank_item_consume_async (proxy, item, NULL, NULL);
}

gboolean
sr_item_is_consumed (const char *item)
{
  gboolean ret;
  org_meego_SourceRank_item_is_consumed (proxy, item, &ret, NULL);
  return ret;
}

glong
sr_item_get_time (const char *item)
{
  gint64 ret;
  org_meego_SourceRank_item_get_time (proxy, item, &ret, NULL);
  return ret;
}

void
sr_item_set_key (const char *item,
                 const char *key,
                 const char *value)
{
  org_meego_SourceRank_item_set_key_async (proxy, item, key, value, NULL, NULL);
}

char *
sr_item_get_key (const char *item,
                 const char *key)
{
  char *ret = NULL;
  org_meego_SourceRank_item_get_key (proxy, item, key, &ret, NULL);
  return ret;
}

char **
sr_list_sources (const char *engine)
{
  char **ret = NULL;
  org_meego_SourceRank_list_sources (proxy, engine, &ret, NULL);
  return ret;
}

char **
sr_list_engines (void)
{
  char **ret = NULL;
  org_meego_SourceRank_list_engines (proxy, &ret, NULL);
  return ret;
}

char **
sr_list_items (const char *engine,
               int         items)
{
  char **ret = NULL;
  org_meego_SourceRank_list_items  (proxy, engine, items, &ret, NULL);
  return ret;
}

char **
sr_source_list_items (const char *engine,
                      const char *source)
{
  char **ret = NULL;
  org_meego_SourceRank_source_list_items (proxy, engine, source, &ret, NULL);
  return ret;
}

void sr_engine_remove (const char *engine)
{
  org_meego_SourceRank_engine_remove_async (proxy, engine, NULL, NULL);
}

void sr_source_remove (const char *engine,
                       const char *source)
{
  org_meego_SourceRank_source_remove_async (proxy, engine, source, NULL, NULL);
}

void sr_item_remove   (const char *item)
{
  org_meego_SourceRank_item_remove_async (proxy, item, NULL, NULL);
}

char **sr_item_list_keys (const char *item)
{
  char **ret;
  org_meego_SourceRank_item_list_keys (proxy, item, &ret, NULL);
  return ret;
}

char **sr_item_list_sources (const char *item)
{
  char **ret;
  org_meego_SourceRank_item_list_sources (proxy, item, &ret, NULL);
  return ret;
}

void sr_engine_set_key (const char *engine,
                        const char *key,
                        const char *value)
{
  org_meego_SourceRank_engine_set_key (proxy, engine, key, value, NULL);
}

char *sr_engine_get_key (const char *engine,
                         const char *key)
{
  char *ret = NULL;
  org_meego_SourceRank_engine_get_key (proxy, engine, key, &ret, NULL);
  return ret;
}

