#ifndef __SOURCERANK_H__
#define __SOURCERANK_H__

#include <glib.h>

void       sr_init                (void);

void       sr_configure           (const char *engine,
                                   const char *param,
                                   float       value);
void       sr_add_item            (const char *engine,
                                   const char *source,
                                   const char *item,
                                   glong       time);
void       sr_add_karma           (const char *item,
                                   float       karma);
void       sr_source_add_karma    (const char *engine,
                                   const char *source,
                                   float       karma);
void       sr_item_consume        (const char *item);

void       sr_engine_set_key      (const char *engine,
                                   const char *key,
                                   const char *value);
char      *sr_engine_get_key      (const char *engine,
                                   const char *key);

void       sr_item_set_key        (const char *item,
                                   const char *key,
                                   const char *value);
void       sr_engine_remove       (const char *engine);
void       sr_source_remove       (const char *engine,
                                   const char *source);
void       sr_item_remove         (const char *item);
char     **sr_list_engines        (void);
char     **sr_list_sources        (const char *engine);
char     **sr_source_list_items   (const char *engine,
                                   const char *source);
char     **sr_list_items          (const char *engine,
                                   int         items);
glong      sr_item_get_time       (const char *item);
char      *sr_item_get_key        (const char *item,
                                   const char *key);
char     **sr_item_list_sources   (const char *item);
char     **sr_item_list_keys      (const char *item);

gboolean   sr_item_is_consumed    (const char *item);

#endif
