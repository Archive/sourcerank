#include <sourcerank.h>
#include <glib-object.h>

#define MAXITEMS 100

int
main (int argc, char **argv)
{

  g_type_init (); /* must be done before initializing sourcerank */
  sr_init ();     /* initialize sourcerank */

  char **engines = sr_list_engines ();
  for (int e = 0; engines && engines[e]; e++)
    {
      g_print ("Engine: '%s'\n", engines[e]);
      char **sources = sr_list_sources (engines[e]);
      for (int s = 0; sources && sources[s]; s++)
        {
          g_print (" Source: '%s'\n", sources[s]);
          char **items = sr_source_list_items (engines[e], sources[s]);
          for (int i = 0; items && items[i] && i<MAXITEMS; i++)
            {
              g_print ("    %s %s %li\n", items[i],
                       sr_item_is_consumed(items[i])?"consumed":"",
                       sr_item_get_time (items[i]));
              char **keys = sr_item_list_keys (items[i]);
              for (int k = 0; keys && keys[k]; k++)
                {
                  char *value = sr_item_get_key (items[i], keys[k]);
                  g_print ("       %s = %s\n", keys[k], value);
                  g_free (value);
                }
              g_strfreev (keys);
            }
          g_strfreev (items);
        }
      g_print ("\n");
      g_strfreev (sources);
    }
  g_strfreev (engines);

return 0;
}


