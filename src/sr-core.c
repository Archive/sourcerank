#include <glib.h>

#include <glib-object.h>
#include <dbus/dbus-glib-bindings.h>
#include <stdio.h>
#include <math.h>
#include <gio/gio.h>
#include <string.h>

#define SR_TYPE_CONTEXT sr_context_get_type()

#define SR_CONTEXT(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  SR_TYPE_CONTEXT, SRContext))

#define SR_CONTEXT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  SR_TYPE_CONTEXT, SRContextClass))

#define SR_IS_CONTEXT(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  SR_TYPE_CONTEXT))

#define SR_IS_CONTEXT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  SR_TYPE_CONTEXT))

#define SR_CONTEXT_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  SR_TYPE_CONTEXT, SRContextClass))

typedef struct _SRContext SRContext;
typedef struct _SRContextClass SRContextClass;

struct _SRContext
{
  GObject parent;
  GList  *engines;
  gchar  *path;
};

struct _SRContextClass
{
  GObjectClass     parent_class;
  DBusGConnection *connection;
};

SRContext *sr_context_new (const gchar *path);


G_DEFINE_TYPE (SRContext, sr_context, G_TYPE_OBJECT)


static void
sr_context_init (SRContext *self)
{
  GError *error = NULL;
  DBusGProxy *driver_proxy;
  SRContextClass *klass = SR_CONTEXT_GET_CLASS (self);
  unsigned int request_ret;

  dbus_g_connection_register_g_object (klass->connection,
                                       "/org/meego/SourceRank",
                                       G_OBJECT (self));

  driver_proxy = dbus_g_proxy_new_for_name (klass->connection,
                                            DBUS_SERVICE_DBUS,
                                            DBUS_PATH_DBUS,
                                            DBUS_INTERFACE_DBUS);

  if(!org_freedesktop_DBus_request_name (driver_proxy,
                                         "org.meego.SourceRank",
                                         0, &request_ret,
                                         &error))
    {
      g_warning("Unable to register service: %s", error->message);
      g_error_free (error);
    }
  g_object_unref (driver_proxy);
}

typedef struct SREngine
{
  const char *name;  /* interned string */
  GHashTable *config;
  SRContext  *context;
  GList      *sources;
  GHashTable *items;
  GHashTable *keys;
} SREngine;

typedef struct SRSource
{
  const char *name;  /* interned string */
  GList      *items;      
  GList      *karmas_unread;
  GList      *karmas_read;
  SREngine   *engine;

  /****/
  float       karma_unread; /* cached values */
  float       karma_read;   /* cached values */
  glong       cached_time;
  gboolean    stale;

  long        unread_count;  /* A count of the number of unread items,
                                needs to be kept in sync, used to
                                keep computation fast 
                              */
} SRSource;

typedef struct SRKarma
{
  glong  time;
  float  amount;
} SRKarma;

typedef struct SRItem
{
  glong       time;
  char       *name;
  gboolean    consumed;
  GHashTable *keys;
  GList      *sources;
} SRItem;

#define SR_ITEM(a)    ((SRItem*)(a))
#define SR_SOURCE(a)  ((SRSource*)(a))
#define SR_ENGINE(a)  ((SREngine*)(a))

static void sr_core_load (SRContext *context);
static char *sr_unescape (const char *input);
static char *sr_escape (const char *input);

static guint save_task = 0;

static glong current_time (void)
{
  GTimeVal timeval;
  g_get_current_time (&timeval);
  return timeval.tv_sec;
}

static float
sr_engine_get_config (SREngine *engine,
                      const char *param)
{
  union {gfloat f; gint32 i;} u;
  param = g_intern_string (param);
  u.i = GPOINTER_TO_INT (g_hash_table_lookup (engine->config, param));
  return u.f;
}

static gboolean save_task_cb (gpointer data);

void
sr_core_configure (void       *am,
                   const char *engine_name,
                   const char *param,
                   float       value);

/* called whenever changes are made to the database */
static void dirtied (SRContext *context)
{
  if (!save_task)
    save_task = g_timeout_add (20000, save_task_cb, context);
}


static float
sumup (SRSource *source,
       glong     time,
       GList    *karmas)
{
  float res = 0.0;
  GList *l;
  GTimeVal time_val = {time, 0};
  GDate *date = g_date_new ();
  gint daypos, weekpos, minpos;
  gfloat dayconfig, weekconfig, minconfig;
  gfloat dayfactor, weekfactor, minfactor;

  g_date_set_time_val (date, &time_val);
  weekpos = g_date_get_weekday (date) - 1;
  minpos = time % 60;
  daypos = (time % (60 * 60 * 24))/60;

  dayconfig = sr_engine_get_config (source->engine, "time-of-day-importance");
  weekconfig = sr_engine_get_config (source->engine, "time-of-week-importance");
  minconfig = sr_engine_get_config (source->engine, "time-of-minute-importance");
  dayfactor = weekfactor = minfactor = 1.0;

  for (l = karmas; l; l = l->next)
    {
      SRKarma *karma = l->data;
      gint pdaypos, pweekpos, pminpos;
      gint daydiff, weekdiff, mindiff;

      time_val.tv_sec = karma->time;
      g_date_set_time_val (date, &time_val);

      if (minconfig >= 0.001)
        {
          pminpos = karma->time % 60;
          mindiff = pminpos - minpos;
          if (mindiff > 30) mindiff -= 60;
          minfactor = 1.0 - fabs (mindiff)/30.0;
          minfactor *= minconfig;
        }

      if (dayconfig >= 0.001)
        {
          pdaypos = (karma->time % (60 * 60 * 24))/60;
          daydiff = pdaypos - daypos;
          if (daydiff > 12*60) daydiff -= 24 * 60;
          dayfactor = 1.0 - fabs (daydiff)/(12 * 60.0);
          dayfactor *= dayconfig;
        }

      if (weekconfig >= 0.001)
        {
          pweekpos = g_date_get_weekday (date) - 1;
          weekdiff = pweekpos - weekpos;
          if (weekdiff > 3) weekdiff -= 7;
          weekfactor = 1.0 - fabs (weekdiff)/(7/2.0);
          weekfactor *= weekconfig;
        }

      res += karma->amount * dayfactor * weekfactor * minfactor;
    }
  g_date_free (date);
  return res;
}

static float compute (SRSource *source, float karma)
{
  if (fabs (karma - 0.0) < 0.00001)
    {
      if (source->items == NULL)
        return 0.0;
      else
        return 1.0 / g_list_length (source->items);
    }
  else
    {
      if (karma > g_list_length (source->items))
        return 1.0; /* ceiling of how good a stickiness can be */
      else
        return karma / g_list_length (source->items);
    }
}

static void sr_source_update_karma (SRSource *source, glong time)
{
  source->karma_unread = compute (source, sumup (source, time, source->karmas_unread));
  source->karma_read = compute (source, sumup (source, time, source->karmas_read));
  source->cached_time = time;
  source->stale = FALSE;
}

float sr_source_karma_read (SRSource *source, glong time)
{
  if (source->stale || source->cached_time != time)
    sr_source_update_karma (source, time);
  return source->karma_read;
}

float sr_source_karma_unread (SRSource *source, glong time)
{
  if (source->stale || source->cached_time != time)
    sr_source_update_karma (source, time);
  return source->karma_unread;
}

static SRKarma *sr_karma_new (SRSource   *source,
                              gboolean    read,
                              glong       time,
                              gfloat      amount)
{
  SRKarma *karma = g_slice_new0 (SRKarma);
  karma->time = time;
  karma->amount = amount;
  if (!read)
    source->karmas_unread = g_list_prepend (source->karmas_unread, karma);
  else
    source->karmas_read = g_list_prepend (source->karmas_read, karma);
  source->stale = TRUE;
  return karma;
}

static void sr_karma_free (SRKarma *karma)
{
  g_slice_free (SRKarma, karma);
}

static gint sr_item_compare_time (gconstpointer a,
                                  gconstpointer b)
{
  const SRItem *itema = a , *itemb = b;
  return itemb->time - itema->time;
}

static const gchar *sr_item_get_key2 (SRItem *item,
                                      const gchar *key)
{
  if (!item->keys)
    return NULL;
  key = g_intern_string (key);
  return g_hash_table_lookup (item->keys, key);
}

static void sr_item_set_key2 (SRItem      *item,
                              const gchar *key,
                              const gchar *value)
{
  key = g_intern_string (key);
  if (!item->keys)
    item->keys = g_hash_table_new_full (g_direct_hash, g_direct_equal,
                                        NULL, g_free);
  g_hash_table_insert (item->keys, (void *)key, g_strdup (value));
}

static SRItem *sr_item_new (SRSource   *source,
                            const char *item_name,
                            glong       time)
{
  SRItem *item = g_slice_new0 (SRItem);
  item->time = time;
  item->name = g_strdup (item_name);
  item->sources = g_list_prepend (NULL, source);
  source->items = g_list_insert_sorted (source->items, item,
                                        sr_item_compare_time);
  g_hash_table_insert (source->engine->items,
                       item->name, item);
  source->unread_count ++;
  return item;
}

static void sr_item_free (SRSource *source, 
                          SRItem   *item)
{
  item->sources = g_list_remove (item->sources, source);
  if (!item->consumed)
    source->unread_count --;

  if (item->sources == NULL)
    {
      g_hash_table_remove (source->engine->items, item->name);
      if (item->keys)
        g_hash_table_destroy (item->keys);
      g_free (item->name);
      g_slice_free (SRItem, item);
    }
}

/* look for an item in a specific source */
static SRItem *sr_find_item (SRSource   *source,
                             const char *item_name)
{
  GList *i;
  for (i = source->items; i; i=i->next)
    {
      SRItem *item = i->data;
      if (g_str_equal (item->name, item_name))
        return item;
    }
  return NULL;
}

/* look for an item in in all sources, optionally also returning the source
 * it was found in, this should be replaced with an engine global hash for
 * the items to reduce cpu usage needed to look up items.
 */
static SRItem *sr_locate_item (SRContext   *context,
                               const char  *item_name)
{
  GList *e;
  for (e = context->engines; e; e = e->next)
    {
      SREngine *engine = e->data;
      SRItem *item;
      if ((item = g_hash_table_lookup (engine->items, item_name)))
        return item;
    }
  return NULL;
}

/* ensure that an item exist, and that it exist for the given source,
 * time is only used for constructing new items.
 */
static SRItem *sr_ensure_item (SRSource   *source,
                               const char *item_name,
                               glong time)
{
  SRItem *item = NULL;

  /* look for item in other sources */
  GList *s;
  for (s = source->engine->sources; s; s = s->next)
    {
      SRSource *source2 = s->data;
      if (source2 != source && (item = sr_find_item (source2, item_name)))
        {
          if (!g_list_find (item->sources, source))
            {
              item->sources = g_list_prepend (item->sources, source);
              source->items = g_list_insert_sorted (source->items, item,
                                                    sr_item_compare_time);
              if (!item->consumed)
                source->unread_count++;
            }
          return item;
        }
    }

  if ((item = sr_find_item (source, item_name)))
    return item;
  return sr_item_new (source, item_name, time);
}

static SRSource *sr_source_new (SREngine   *engine,
                                const char *source_name)
{
  SRSource *source = g_slice_new0 (SRSource);
  source->name = source_name;
  source->stale = TRUE;
  source->items = NULL;

  engine->sources = g_list_prepend (engine->sources, source);
  source->engine = engine;
  return source;
}

static void sr_source_free (SRSource *source)
{
  GList *i;
  for (i = source->items; i; i=i->next)
    sr_item_free (source, i->data);
  g_list_free (source->items);
  for (i = source->karmas_unread; i; i=i->next)
    sr_karma_free (i->data);
  g_list_free (source->karmas_unread);
  for (i = source->karmas_read; i; i=i->next)
    sr_karma_free (i->data);
  g_list_free (source->karmas_read);

  g_slice_free (SRSource, source);
}

SRSource *sr_ensure_source (SREngine *engine, const char *source_name)
{
  GList *s;
  source_name = g_intern_string (source_name);
  for (s = engine->sources; s; s=s->next)
    {
      SRSource *source = s->data;
      if (source->name == source_name)
        return source;
    }
  return sr_source_new (engine, source_name);
}

static SREngine *sr_engine_new (SRContext  *context,
                                const char *engine_name)
{
  SREngine *engine = g_slice_new0 (SREngine);
  engine->name = engine_name;

  engine->context = context;
  engine->config = g_hash_table_new (g_direct_hash, g_direct_equal);
  engine->items = g_hash_table_new (g_str_hash, g_str_equal);
  context->engines = g_list_append (context->engines, engine);

  /* default configuration */
  sr_core_configure (context, engine_name, "full-summary", 1.0);
  sr_core_configure (context, engine_name, "summary-ratio", 0.5);
  sr_core_configure (context, engine_name, "consumed-mixin", 0.0); 
  sr_core_configure (context, engine_name, "unconsumed-mixin", 0.0);
  sr_core_configure (context, engine_name, "time-of-day-importance", 0.0);
  sr_core_configure (context, engine_name, "time-of-week-importance", 0.0);
  sr_core_configure (context, engine_name, "days-to-keep-items", 30.0);
  sr_core_configure (context, engine_name, "days-to-keep-karma", 30.0);

  return engine;
}

void
sr_engine_free (SREngine *engine)
{
  GList *s;
  for (s = engine->sources; s; s=s->next)
    sr_source_free (s->data);
  g_list_free (engine->sources);
  g_hash_table_destroy (engine->config);
  g_assert (g_hash_table_size (engine->items) == 0);
  g_hash_table_destroy (engine->items);
  if (engine->keys)
    g_hash_table_destroy (engine->keys);
  g_slice_free (SREngine, engine);
}

SREngine *
sr_ensure_engine (SRContext  *context,
                  const char *engine_name)
{
  GList *e;
  engine_name = g_intern_string (engine_name);
  for (e = context->engines; e; e=e->next)
    {
      SREngine *engine = e->data;
      if (engine->name == engine_name)
        return engine;
    }
  return sr_engine_new (context, engine_name);
}

void
sr_core_configure (void       *am,
                   const char *engine_name,
                   const char *param,
                   float       value)
{
  SREngine *engine = sr_ensure_engine (am, engine_name);
  union {gfloat f; gint32 i;} u;
  u.f = value;
  param = g_intern_string (param);
  g_hash_table_insert (engine->config, (void*)param,
                       GINT_TO_POINTER (u.i));
}

void
sr_core_add_item (void       *am,
                  const char *engine_name,
                  const char *source_name,
                  const char *item_name,
                  glong       time)
{
  SREngine *engine = sr_ensure_engine (am, engine_name);
  SRSource *source = sr_ensure_source (engine, source_name);
  SRItem   *item;

  item = sr_ensure_item (source, item_name, time);
  item->time = time;
  source->stale = TRUE;
  dirtied (am);
}

void
sr_core_item_add_karma_with_time (void       *am,
                                  const char *item_name,
                                  float       karma_amount,
                                  glong       time)
{
  SRItem *item;
  if ((item = sr_locate_item (am, item_name)))
    {
      GList *s;
      for (s = item->sources; s; s = s->next)
        {
          SRSource *source = s->data;
          sr_karma_new (source, item->consumed, time, karma_amount);
        }
    }

  dirtied (am);
}

void
sr_core_source_add_karma_with_time (void       *am,
                                    const char *engine_name,
                                    const char *source_name,
                                    float       karma_amount,
                                    glong       time)
{
  SREngine *engine = sr_ensure_engine (am, engine_name);
  SRSource *source = sr_ensure_source (engine, source_name);
  sr_karma_new (source, FALSE, time, karma_amount);
  dirtied (am);
}

void
sr_core_source_add_karma (void       *am,
                          const char *engine_name,
                          const char *source_name,
                          float       karma_amount)
{
  sr_core_source_add_karma_with_time (am, engine_name, source_name,
                                      karma_amount, current_time ());
}

static void
sr_core_source_add_read_karma_with_time (void       *am,
                                         const char *engine_name,
                                         const char *source_name,
                                         float       karma_amount,
                                         glong       time)
{
  SREngine *engine = sr_ensure_engine (am, engine_name);
  SRSource *source = sr_ensure_source (engine, source_name);
  sr_karma_new (source, TRUE, time, karma_amount);
  dirtied (am);
}

void
sr_core_item_consume (void       *am,
                      const char *item_name)
{
  SRItem   *item = sr_locate_item (am, item_name);

  if (!item)
    return;

  if (item->consumed == FALSE)
    {
      GList *s;
      for (s=item->sources; s; s = s->next)
        {
          SRSource *source = s->data;
          source->unread_count --;
        }
    }

  item->consumed = TRUE;
  dirtied (am);
}

void
sr_core_engine_set_key (void       *am,
                        const char *engine_name,
                        const char *key,
                        const char *value)
{
  SREngine *engine = sr_ensure_engine (am, engine_name);
  key = g_intern_string (key);
  if (!engine->keys)
    engine->keys = g_hash_table_new_full (g_direct_hash, g_direct_equal,
                                        NULL, g_free);
  g_hash_table_insert (engine->keys, (void *)key, g_strdup (value));
}

char *
sr_core_engine_get_key (void       *am,
                        const char *engine_name,
                        const char *key)
{
  SREngine *engine = sr_ensure_engine (am, engine_name);
  char *ret;
  if (!engine->keys)
    return NULL;
  key = g_intern_string (key);
  ret = g_hash_table_lookup (engine->keys, key);
  if (ret)
    ret = g_strdup (ret);
  return ret;
}

void
sr_core_item_set_key (void       *am,
                      const char *item_name,
                      const char *key,
                      const char *value)
{
  SRItem *item;
  if ((item = sr_locate_item (am, item_name)))
    {
      sr_item_set_key2 (item, key, value);
      dirtied (am);
    }
}

char *
sr_core_item_get_key (void       *am,
                      const char *item_name,
                      const char *key)
{
  SRItem *item;
  if ((item = sr_locate_item (am, item_name)))
    {
      const char * value = sr_item_get_key2 (item, key);
      if (value)
        return g_strdup (value);
    }
  return NULL;
}

static char **make_array (GList *list)
{
  char **ret_array;
  GList *l = NULL;
  int n_ret, i;
  list = g_list_reverse (list);

  n_ret = g_list_length (list);
  ret_array = g_new (char *, n_ret + 1);

  for (l = list, i = 0; l; l = l->next, i++) {
       ret_array[i] = g_strdup (l->data);
    }

  ret_array[n_ret] = NULL;
  g_list_free (list);
  return ret_array;
}

static glong hackysorttime = 0;

static gint sr_source_compare_karma (gconstpointer a,
                                     gconstpointer b)
{
  float karma_a = sr_source_karma_unread (SR_SOURCE (a), hackysorttime);
  float karma_b = sr_source_karma_unread (SR_SOURCE (b), hackysorttime);

  if (karma_a < karma_b)
    return 1;
  else if (karma_a > karma_b)
    return -1;
  return 0;
}

char **
sr_core_list_sources (void       *am,
                      const char *engine_name)
{
  SREngine *engine = sr_ensure_engine (am, engine_name);
  GList *ret = NULL;
  GList *a;

  hackysorttime = current_time ();
  engine->sources = g_list_sort (engine->sources, 
                                 sr_source_compare_karma);
  for (a = engine->sources; a; a = a->next)
    {
      SRSource *source = a->data;
      ret = g_list_prepend (ret, (void*)source->name);
    }
  return make_array (ret);
}

static void
sr_context_finalize (GObject *object)
{
  SRContext *context = SR_CONTEXT (object);
  GList *e;
  for (e = context->engines; e; e=e->next)
    sr_engine_free (e->data);
  g_list_free (context->engines);
  if (context->path)
    g_free (context->path);


  G_OBJECT_CLASS (sr_context_parent_class)->finalize (object);
}

void sr_core_item_remove   (void       *am,
                            const char *item_name)
{
  SRItem *item;
  /* ensuring we go through all engines */
  while ((item = sr_locate_item (am, item_name)))
    {
      GList *s;
      for (s = item->sources; s; s = s->next)
        {
          SRSource *source = s->data;
          source->items = g_list_remove (source->items, item);
          sr_item_free (source, item);
        }
    }
}

static void sr_core_load (SRContext *context)
{
  GFile *file;
  GInputStream *i;//, *old;
  //GConverter *converter;
  file = g_file_new_for_path (context->path);
  //converter = (GConverter*)g_zlib_decompressor_new(G_ZLIB_COMPRESSOR_FORMAT_GZIP);

  i = G_INPUT_STREAM (g_file_read (file, FALSE, NULL));

  if (i)
    {
      //old = i;
      //i = (GInputStream*) g_converter_input_stream_new (i, converter);
      //g_object_unref (old);
      gchar *engine_name = NULL, *source_name = NULL, *item_name = NULL;

      GString *str = g_string_new ("");
      gchar  buf [4096];
      gssize read = 0;
      gboolean gotitem = FALSE;

      while ((read = g_input_stream_read (G_INPUT_STREAM (i),
                                          buf, 4096, FALSE, NULL))>0)
        {
          gssize p;
          for (p = 0; p < read; p++)
            {
              if (buf[p] != '\n')
                g_string_append_c (str, buf[p]);
              else
                {
                  if (g_str_has_prefix (str->str, "Engine: "))
                    {
                      if (engine_name)
                        g_free (engine_name);
                      engine_name = sr_unescape (strstr (str->str, ": ") + 2);
                    }
                  else if (g_str_has_prefix (str->str, "config: "))
                    {
                      gchar *tmp = g_strdup (strstr (str->str, ": ") + 2);
                      gchar *tmp2;
                      gchar *value = strchr (tmp, '=');
                      *value = '\0';
                      value++;

                      tmp2 = sr_unescape (tmp);
                      sr_core_configure (context, engine_name,
                                                tmp2, g_strtod (value, NULL));

                      g_free (tmp2);
                      g_free (tmp);
                    }
                  else if (g_str_has_prefix (str->str, "Source: "))
                    {
                      if (source_name)
                        g_free (source_name);
                      source_name = sr_unescape (strstr (str->str, ": ") + 2);
                      gotitem = FALSE;
                    }
                  else if (g_str_has_prefix (str->str, "Item: "))
                    {
                      gotitem = TRUE;
                      if (item_name)
                        g_free (item_name);
                      item_name = sr_unescape (strstr (str->str, ": ") +2);
                    }
                  else if (g_str_has_prefix (str->str, "ekey: "))
                    {
                      gchar *tmp = g_strdup (strstr (str->str, ": ") + 2);
                      gchar *ktmp, *vtmp;
                      gchar *value = strchr (tmp, '=');
                      *value = '\0';
                      value++;
                      ktmp = sr_unescape (tmp);
                      vtmp = sr_unescape (value);
                      sr_core_engine_set_key (context,
                                              engine_name, ktmp, vtmp);

                      g_free (ktmp);
                      g_free (vtmp);
                      g_free (tmp);
                    }
                  else if (g_str_has_prefix (str->str, "key: "))
                    {
                      gchar *tmp = g_strdup (strstr (str->str, ": ") + 2);
                      gchar *ktmp, *vtmp;
                      gchar *value = strchr (tmp, '=');
                      *value = '\0';
                      value++;
                      ktmp = sr_unescape (tmp);
                      vtmp = sr_unescape (value);
                      sr_core_item_set_key (context,
                                            item_name, ktmp, vtmp);

                      g_free (ktmp);
                      g_free (vtmp);
                      g_free (tmp);
                    }
                  else if (g_str_has_prefix (str->str, "readkarma: "))
                    {
                      const gchar *start = str->str + 11;
                      gfloat amount = 0.0;
                      glong time = 0;
                      sscanf (start, "%li %f", &time, &amount);
                      sr_core_source_add_read_karma_with_time (context,
                                                               engine_name,
                                                               source_name,
                                                               amount,
                                                               time);
                    }
                  else if (g_str_has_prefix (str->str, "unreadkarma: "))
                    {
                      glong time = 0;
                      gfloat amount = 0.0;
                      const gchar *start = str->str + 13;
                      sscanf (start, "%li %f", &time, &amount);
                      sr_core_source_add_karma_with_time (context,
                                                          engine_name,
                                                          source_name,
                                                          amount,
                                                          time);
                    }
                  else if (gotitem)
                    {
                      glong time = 0;
                      gint  consumed = 0;
                      sscanf (str->str, "%li %i", &time, &consumed);

                      sr_core_add_item (context,
                                        engine_name,
                                        source_name,
                                        item_name,
                                        time);
                      if (consumed)
                        sr_core_item_consume (context, item_name);
                    }
                  g_string_truncate (str, 0);
                }
            }
        }
      if (engine_name)
        g_free (engine_name);
      if (source_name)
        g_free (source_name);
      if (item_name)
        g_free (item_name);
      g_string_free (str, TRUE);
      g_object_unref (i);
    }
  g_object_unref (file);
  //g_object_unref (converter);
}

static char *sr_escape (const char *input)
{
  GString *str = g_string_new ("");
  char *ret;
  for (; input && *input !='\0'; input++)
    {
      switch (*input)
        {
          case '=': g_string_append (str, "\\e"); break;
          case '\n': g_string_append (str, "\\n"); break;
          case '\\': g_string_append (str, "\\\\"); break;
          default: g_string_append_c (str, *input); break;
        }
    }
  ret = str->str;
  g_string_free (str, FALSE);
  return ret;
}

static char *sr_unescape (const char *input)
{
  GString *str = g_string_new ("");
  char *ret;
  for (; *input !='\0'; input++)
    {
      switch (*input)
        {
          case '\\':
            input++;
            switch (*input)
              {
                case 'e': g_string_append_c (str, '='); break;
                case 'n': g_string_append_c (str, '\n'); break;
                case '\0': input--; break;
                default: g_string_append_c (str, *input); break;
              }
            break;
          default: g_string_append_c (str, *input); break;
        }
    }
  ret = str->str;
  g_string_free (str, FALSE);
  return ret;
}

static void sr_core_save (SRContext *context)
{
  GFile *tmpfile, *file, *parent;
  GOutputStream *o;//, *old;
  //GConverter *converter;

  GError *error = NULL;
  gchar *path;
  if (!context->path)
    return;

  //converter = (GConverter*)g_zlib_compressor_new(G_ZLIB_COMPRESSOR_FORMAT_GZIP, -1);

  file = g_file_new_for_path (context->path);
  path = g_strdup_printf ("%s~", context->path);
  tmpfile = g_file_new_for_path (context->path);
  g_free (path);

  parent = g_file_get_parent (file);
  path = g_file_get_path (parent);
  g_mkdir_with_parents (path, 488);
  g_object_unref (parent);

  g_file_delete (file, FALSE, NULL);
  o = (GOutputStream*) g_file_create (tmpfile, G_FILE_CREATE_NONE, FALSE, &error);
  //old = o;
  //o = (GOutputStream*) g_converter_output_stream_new (G_OUTPUT_STREAM (o), converter);
  //g_object_unref (old);

  if (error)
    {
      g_warning ("%s: %s", G_STRLOC, error->message);
      return;
    }

#define FLUSH \
  g_output_stream_write_all (o, str->str, str->len, NULL, FALSE, &error);\
  g_string_truncate (str, 0);

  {
    GString *str = g_string_new ("");
    GList *e;
    g_string_append_printf (str, "SourceRank: %s\n", context->path);
    for (e = context->engines; e; e= e->next)
      {
        SREngine *engine = e->data;
        GHashTableIter iter;
        gpointer key, value;
        GList *s;
    
        {
          char *tmp = sr_escape (engine->name);
          g_string_append_printf (str, "Engine: %s\n", tmp);
          g_free (tmp);
        }

        g_hash_table_iter_init (&iter, engine->config);
        while (g_hash_table_iter_next (&iter, &key, &value)) 
          { 
            char *tmp = sr_escape ((char *)key);
            g_string_append_printf (str, "config: %s=%f\n", tmp,
                                    sr_engine_get_config(engine, key));
            g_free (tmp);
          }

        if (engine->keys)
          {
            GHashTableIter iter;
            gpointer key, value;
            g_hash_table_iter_init (&iter, engine->keys);
            while (g_hash_table_iter_next (&iter, &key, &value)) 
              {
                char *tmp = sr_escape (key);
                char *tmp2 = sr_escape (value);
                g_string_append_printf (str, "ekey: %s=%s\n", tmp, tmp2);
                g_free (tmp);
                g_free (tmp2);
              }
          }

        for (s = engine->sources; s; s= s->next)
          {
            GList *i;
            GList *k;
            SRSource *source = s->data;

            {
              char *tmp = sr_escape (source->name);
              g_string_append_printf (str, "Source: %s\n", tmp);
              g_free (tmp);
            }
            for (i = source->items; i; i= i->next)
              {
                SRItem *item = i->data;
                char *tmp = sr_escape (item->name);
                g_string_append_printf (str, "Item: %s\n", tmp);
                g_free (tmp);
                g_string_append_printf (str, "  %li %i\n", item->time,
                                                           item->consumed);

                if (item->keys)
                  {
                    GHashTableIter iter;
                    gpointer key, value;
                    g_hash_table_iter_init (&iter, item->keys);
                    while (g_hash_table_iter_next (&iter, &key, &value)) 
                      {
                        char *tmp = sr_escape (key);
                        char *tmp2 = sr_escape (value);
                        g_string_append_printf (str, "key: %s=%s\n", tmp, tmp2);
                        g_free (tmp);
                        g_free (tmp2);
                      }
                  }

                FLUSH;
              }
            for (k = source->karmas_unread; k; k= k->next)
              {
                SRKarma *karma = k->data;
                g_string_append_printf (str, "unreadkarma: %li %f\n",
                                        karma->time, karma->amount);
                FLUSH;
              }
            for (k = source->karmas_read; k; k= k->next)
              {
                SRKarma *karma = k->data;
                g_string_append_printf (str, "readkarma: %li %f\n", karma->time,
                                        karma->amount);
                FLUSH;
              }
            FLUSH;
          }
      }

    if (error)
      {
        g_warning ("%s: %s", G_STRLOC, error->message);
        return;
      }
    g_string_free (str, TRUE);

  }

  g_output_stream_flush (o, NULL, NULL);
  //g_object_unref (converter);
  g_object_unref (o); // XXX: causes a crash
  g_printf ("Wrote\n");

  g_file_move (tmpfile, file, G_FILE_COPY_OVERWRITE, NULL, NULL, NULL, NULL);

  g_object_unref (file);
  g_object_unref (tmpfile);
}

static gboolean save_task_cb (gpointer data)
{
  SRContext *context = data;
  glong curr_time = current_time ();

  /* remove any too old items */
  {GList *e;
   for (e = context->engines; e; e = e->next)
     {
       SREngine *engine = e->data;
       GList *s;
       int seconds_to_keep_items =
         sr_engine_get_config (engine, "days-to-keep-items") * 60 * 60 * 24;
       int seconds_to_keep_karma =
         sr_engine_get_config (engine, "days-to-keep-karma") * 60 * 60 * 24;
       
       for (s=engine->sources; s; s = s->next)
         {
           SRSource *source = s->data;
           GList *i;
items_again:
           for (i = source->items; i; i = i->next)
             {
               SRItem *item = i->data;
               if (curr_time - item->time > seconds_to_keep_items)
                 {
                   sr_core_item_remove (context, item->name);
                   goto items_again;
                 }
             }
karma_again:
           for (i = source->karmas_read; i; i = i->next)
             {
               SRKarma *karma = i->data;
               if (curr_time - karma->time > seconds_to_keep_karma)
                 {
                   source->karmas_read = g_list_remove (source->karmas_read,
                                                        karma);
                   sr_karma_free (karma);
                   goto karma_again;
                 }
             }
karma_again2:
           for (i = source->karmas_unread; i; i = i->next)
             {
               SRKarma *karma = i->data;
               if (curr_time - karma->time > seconds_to_keep_karma)
                 {
                   source->karmas_unread = g_list_remove (source->karmas_unread,
                                                          karma);
                   sr_karma_free (karma);
                   goto karma_again2;
                 }
             }

         }
     }
  }
  sr_core_save (context);

  save_task = 0;
  return FALSE;
}

/*
 * Returns a list of item names
 */
char **
sr_core_list_items (void       *am,
                    const char *engine_name,
                    int         items)
{
  GList *result = NULL;
  int    result_length = 0;
  SREngine *engine = sr_ensure_engine (am, engine_name);
  GList *s;
  int slots = items;
  glong time = current_time ();
  int hotspots = slots * sr_engine_get_config (engine, "summary-ratio");
  int all_sources_unread = 0;
  float karma_unread_sum = 0.0;
  float karma_read_sum = 0.0;
  float used_slots = 0.0;
  float try_slots;
  float slot[1024]={0.0,}; /* XXX: limited to a maximum of 1024 sources */

  gboolean show_all_cats;

  show_all_cats = sr_engine_get_config (engine, "full-summary")>0.1?TRUE:FALSE;

  if (g_list_length (engine->sources) > 1024)
    return NULL;

  for (s = engine->sources; s; s = s->next)
    {
      SRSource *source = s->data;
      karma_unread_sum += sr_source_karma_unread (source, time);
      karma_read_sum += sr_source_karma_read (source, time);
      all_sources_unread += source->unread_count;
    }

  if (karma_read_sum < 0.1)
    karma_read_sum = 1.0;

  if (karma_unread_sum < 0.1)
    karma_unread_sum = 1.0;

  hackysorttime = time;
  engine->sources = g_list_sort (engine->sources, 
                                 sr_source_compare_karma);
  
  /* Reduce number of hotspots if no items are available */
  if (hotspots > all_sources_unread)
    hotspots = all_sources_unread;

  try_slots = hotspots;

  /* Compute how many items of each type to show in summary */
  while (used_slots < hotspots)
    {
      int no;
      for (s = engine->sources, no = 0; s; s = s->next, no++)
        {
          SRSource *source = s->data;
          slot[no] = 
            try_slots * sr_source_karma_unread (source, time)/karma_unread_sum;
          if (slot[no] < 1.0)
            { if (show_all_cats)
                slot[no] = 1;
              else if (slot[no] < 0.0)
                slot[no] = 0;
            }
        }

      used_slots = 0;

      for (s = engine->sources, no = 0; s; s = s->next, no++)
        {
          SRSource *source = s->data;
          int items = source->unread_count;
          
          if (items > slot[no])
            {
              used_slots += slot[no];
            }
          else
            {
              used_slots += items;
            }
        }
      try_slots += 1;
    }

  /* Fill summary */
    {
      int no = 0;
      for (s = engine->sources, no = 0; s; s = s->next, no++)
      {
        SRSource *source = s->data;
        GList *itemi;
        int collected = 0;
        for (itemi = source->items; itemi; itemi = itemi->next)
          {
            SRItem *item = itemi->data;
            if (!item->consumed && !g_list_find (result, item->name))
              {
                result = g_list_prepend (result, item->name);
                result_length ++;
                collected ++;
              }
            if (collected >= slot[no])
              break;
          }
        slot[no] -= floor (slot[no]);
      }
    }

  /* backlog */
  {
    int i;
    for (i = 0; i < 500; i++)
      {
        int no;
        for (s = engine->sources, no = 0; s; s = s->next, no++)
          {
            SRSource *source = s->data;
            slot[no] += sr_source_karma_read (source, time) / karma_read_sum;
            if (! (slot[no] > 0 && slot[no] < 400.0))
              slot[no] = 0;
            if (slot[no] > 1.0 && result_length < items)
              {
                GList *t;
                for (t = source->items; t; t = t->next)
                  {
                    SRItem *item = t->data;
                    if (!g_list_find (result, item->name))
                      {
                        result = g_list_prepend (result, item->name);
                        result_length ++;
                        slot[no] -= 1;
                        if (slot[no] < 1.0)
                          goto filled_source;
                      }
                  }
              }
            filled_source:;
          }

        if (result_length >= items)
          break;
      }
  }

  return make_array (result);
}

/*********************** start of dbus methods ************************/

gboolean
sr_context_configure (SRContext   *context,
                      const gchar *engine_name,
                      const gchar *setting,
                      gdouble      value,
                      GError     **error)
{
  sr_core_configure (context, engine_name, setting, value);
  return TRUE;
}

gboolean
sr_context_add_item (SRContext   *context,
                     const gchar *engine_name,
                     const gchar *source_name,
                     const gchar *item,
                     gint64       time,
                     GError     **error)
{
  sr_core_add_item (context, engine_name, source_name, item, time);
  return TRUE;
}

gboolean
sr_context_item_get_time (SRContext   *context,
                          const gchar *item_name,
                          gint64      *time,
                          GError     **error)
{
  SRItem *item;
  if ((item = sr_locate_item (context, item_name)))
    {
      * time = item->time;
    }
  else
    * time = 0;
  return TRUE;
}

gboolean
sr_context_item_get_key (SRContext   *context,
                         const gchar *item,
                         gchar       *key,
                         gchar      **value,
                         GError     **error)
{
  if (sr_core_item_get_key (context, item, key))
    *value = sr_core_item_get_key (context, item, key);
  else
    *value = NULL;
  return TRUE;
}

gboolean
sr_context_item_set_key (SRContext   *context,
                         const gchar *item,
                         gchar       *key,
                         gchar       *value,
                         GError     **error)
{
  sr_core_item_set_key (context, item, key, value);
  return TRUE;
}


gboolean
sr_context_engine_get_key (SRContext   *context,
                           const gchar *engine,
                           gchar       *key,
                           gchar      **value,
                           GError     **error)
{
  if (sr_core_engine_get_key (context, engine, key))
    *value = sr_core_engine_get_key (context, engine, key);
  else
    *value = NULL;
  return TRUE;
}

gboolean
sr_context_engine_set_key (SRContext   *context,
                           const gchar *engine,
                           gchar       *key,
                           gchar       *value,
                           GError     **error)
{
  sr_core_engine_set_key (context, engine, key, value);
  return TRUE;
}


gboolean
sr_context_item_add_karma_with_time (SRContext   *context,
                                     const gchar *item_name,
                                     gdouble      amount,
                                     gint64       time,
                                     GError     **error)
{
  sr_core_item_add_karma_with_time (context, item_name, amount, time);
  return TRUE;
}

gboolean
sr_context_item_add_karma (SRContext   *context,
                           const gchar *item_name,
                           gdouble      amount,
                           GError     **error)
{
  sr_core_item_add_karma_with_time (context, item_name, amount,
                                    current_time ());
  return TRUE;
}

gboolean
sr_context_source_add_karma (SRContext  *context,
                             const gchar *engine_name,
                             const gchar *source_name,
                             gdouble      amount,
                             GError     **error)
{
  sr_core_source_add_karma (context, engine_name, source_name, amount);
  return TRUE;
}



gboolean
sr_context_item_consume (SRContext   *context,
                         const gchar *item,
                         GError     **error)
{
  sr_core_item_consume (context, item);
  return TRUE;
}


gboolean
sr_context_item_is_consumed (SRContext   *context,
                             const gchar *item_name,
                             gboolean    *consumed,
                             GError     **error)
{
  SRItem *item;
  if ((item = sr_locate_item (context, item_name)))
    *consumed = item->consumed;
  else
    *consumed = FALSE;
  return TRUE;
}

gboolean
sr_context_list_items (SRContext   *context,
                       const gchar *engine_name,
                       gint         count,
                       char      ***ret,
                       GError     **error)
{
  *ret = sr_core_list_items (context, engine_name, count);
  return TRUE;
}

gboolean
sr_context_source_list_items (SRContext   *context,
                              const gchar *engine_name,
                              const gchar *source_name,
                              char      ***ret,
                              GError     **error)
{
  SREngine *engine = sr_ensure_engine (context, engine_name);
  SRSource *source = sr_ensure_source (engine, source_name);
  GList *lret = NULL;
  GList *a;
  if (!source)
    return FALSE;
  for (a = source->items; a; a = a->next)
    {
      SRItem *item = a->data;
      lret = g_list_prepend (lret, (void*)item->name);
    }
  * ret = make_array (lret);
  return TRUE;
}


gboolean
sr_context_item_list_keys (SRContext   *context,
                           const gchar *item_name,
                           char      ***ret,
                           GError     **error)
{
  SRItem   *item = sr_locate_item (context, item_name);
  if (item && item->keys)
    {
      GList *keys;
      keys = g_hash_table_get_keys (item->keys);
      * ret = make_array (keys);
    }
  return TRUE;
}


gboolean
sr_context_item_list_sources (SRContext  *context,
                              const gchar *item_name,
                              char      ***ret,
                              GError     **error)
{
  SRItem   *item = sr_locate_item (context, item_name);
  if (item)
    {
      GList *iter = NULL;
      GList *retl = NULL;
      for (iter = item->sources; iter; iter=iter->next)
        {
          retl = g_list_append (retl, (void*)((SRSource*)iter->data)->name);
        }
      * ret = make_array (retl);
    }
  return TRUE;
}

gboolean
sr_context_item_remove (SRContext   *context,
                        const gchar *item,
                        GError     **error)
{
  sr_core_item_remove (context, item);
  return TRUE;
}


gboolean
sr_context_source_remove (SRContext  *context,
                          const gchar *engine_name,
                          const gchar *source_name,
                          GError     **error)
{
  SREngine *engine = sr_ensure_engine (context, engine_name);
  SRSource *source = sr_ensure_source (engine, source_name);

  sr_source_free (source);
  engine->sources = g_list_remove (engine->sources, source);
  return TRUE;
}

gboolean
sr_context_engine_remove (SRContext   *context,
                          const gchar *engine_name,
                          GError     **error)
{
  SREngine *engine = sr_ensure_engine (context, engine_name);
  sr_engine_free (engine);
  context->engines = g_list_remove (context->engines, engine);
  return TRUE;
}


gboolean
sr_context_list_sources (SRContext   *context,
                         const gchar *engine_name,
                         char      ***ret,
                         GError     **error)
{
  *ret = sr_core_list_sources (context, engine_name);
  return TRUE;
}


gboolean
sr_context_list_engines (SRContext   *context,
                         char      ***ret,
                         GError     **error)
{
  GList *retl = NULL;
  GList *a;
  for (a = context->engines; a; a = a->next)
    {
      SREngine *engine = a->data;
      retl = g_list_prepend (retl, (void*)engine->name);
    }
  *ret = make_array (retl);
  return TRUE;
}

#include "sr-core-glue.h"

static void
sr_context_class_init (SRContextClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GError *error = NULL;
  klass->connection = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
  object_class->finalize = sr_context_finalize;

  if (klass->connection == NULL)
    {
      g_warning("Unable to connect to dbus: %s", error->message);
      g_error_free (error);
      return;
    }

  dbus_g_object_type_install_info (SR_TYPE_CONTEXT, &dbus_glib_sr_context_object_info);
}

SRContext *
sr_context_new (const gchar *path)
{
  SRContext *context = g_object_new (SR_TYPE_CONTEXT, NULL);
  context->path = g_strdup (path);
  sr_core_load (context);
  return context;
}

int
main (int    argc,
      char **argv)
{
  SRContext *server;
  GMainLoop *mainloop;
  char *path;
  g_type_init ();
  path = g_strdup_printf ("%s/sourcerank/db", g_get_user_data_dir ());
  server = sr_context_new (path);
  g_free (path);

  mainloop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (mainloop);

  return 0;
}
