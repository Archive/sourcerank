#include <glib.h>
#include <glib-object.h>
#include "sourcerank.h"

void sr_init_local (const gchar *path); /* spawning the internal
                                           is not public API */

static glong current_time (void)
{
  GTimeVal timeval;
  g_get_current_time (&timeval);
  return timeval.tv_sec;
}

void test (void)
{
  char **engines;
  int e;

#if 1
  sr_add_item ("a", "foo", "5",     current_time ()-18);
  sr_add_item ("a", "bar", "52",    current_time ());
  sr_add_item ("a", "foo", "1",     current_time ()-16);
  sr_add_item ("a", "foo", "sadf1", current_time ()-14);
  sr_add_item ("a", "foo", "2d",    current_time ()-12);
  sr_add_item ("a", "foo", "fa7",   current_time ()-10);
  sr_add_item ("a", "foo", "2s",    current_time ()-8);
  sr_add_item ("a", "foo", "7",     current_time ()-6);
  sr_add_item ("a", "foo", "asdf ", current_time ()-4);
  sr_add_item ("a", "foo", "15",    current_time ()-2);
  sr_add_item ("a", "foo", "6312",  current_time ()-1);
  sr_add_item ("a", "foo", "start time", current_time ()-0);
  sr_add_item ("a", "bar", "start time", current_time ()-0);

  sr_item_consume ("6312");

  sr_add_item ("b", "bbar", "3", 0);
  sr_add_item ("b", "bfoo", "8", 0);
  sr_add_item ("a", "baz", "4", 0);
  sr_add_item ("a", "qux", "4", 0);

  sr_add_karma ("5",  0.2);
  sr_add_karma ("52",  1.0);
  sr_add_karma ("1",  1.0);
  sr_add_karma ("sadf1",  1.0);
  sr_add_karma ("2d", 1.0);
  sr_add_karma ("bbar", 1.5);
  sr_add_karma ("bbar", 1.5);
  sr_add_karma ("bbaz", 1.5);

#endif

  sr_add_item ("a", "foo", "5",     current_time ()-18);
  sr_add_item ("a", "bar", "52",    current_time ());
  sr_add_item ("a", "baz", "1",     current_time ()-16);
  sr_add_item ("a", "foo", "1",     current_time ()-16);

  sr_add_item ("a", "foo", "http://other.blog/?12", 666);

  /*sr_configure ("b", "engine-param-2", 1.3);
  sr_configure ("b", "summary-ratio", 0.5);*/

  engines = sr_list_engines ();
  for (e = 0; engines && engines[e]; e++)
  {
    char **sources;
    int s;
    char **items;
    int i;
    g_print ("Engine: %s\n", engines[e]);

    sources = sr_list_sources (engines[e]);
    for (s = 0; sources && sources[s]; s++)
    {
      g_print ("   %s\n", sources[s]);
    }
    g_strfreev (sources);

#if 1
    g_print (" items:\n");
    //items = sr_source_list_items (l->data, "bar");
    items = sr_list_items (engines[e], 10);
    for (i = 0; items && items[i]; i++)
      g_print ("     %s : %li %s\n", items[i], sr_item_get_time (items[i]),
               sr_item_is_consumed (items[i])?"Y":"N");
    g_strfreev (items);
#endif
  }
  g_strfreev (engines);

  g_print ("done\n\n");
}

int main (int argc, char **argv)
{

  g_type_init ();
  //sr_init_local ("/tmp/aa");
  //test ();
  sr_init ();
  test ();

  return 0;
}
